'use strict';

angular
  .module('docs', [
    'ui.router',
    'app.directive'
  ]);
