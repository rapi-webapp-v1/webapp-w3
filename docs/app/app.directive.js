

angular.module('app.directive', [])

// .directive('runnableExample', [function() {
//   var exampleClassNameSelector = '.runnable-example-file';
//   var tpl =
//     '<nav class="runnable-example-tabs" ng-if="tabs">' +
//     '  <a ng-class="{active:$index==activeTabIndex}"' +
//          'ng-repeat="tab in tabs track by $index" ' +
//          'href="" ' +
//          'class="btn"' +
//          'ng-click="setTab($index)">' +
//     '    {{ tab }}' +
//     '  </a>' +
//     '</nav>';
//
//   return {
//     restrict: 'C',
//     scope : true,
//     controller : ['$scope', function($scope) {
//       $scope.setTab = function(index) {
//         var tab = $scope.tabs[index];
//         $scope.activeTabIndex = index;
//         $scope.$broadcast('tabChange', index, tab);
//       };
//     }],
//     compile : function(element) {
//       element.html(tpl + element.html());
//       return function(scope, element) {
//         var node = element[0];
//         var examples = node.querySelectorAll(exampleClassNameSelector);
//         var tabs = [];
//         angular.forEach(examples, function(child, index) {
//           tabs.push(child.getAttribute('name'));
//         });
//
//         if (tabs.length > 0) {
//           scope.tabs = tabs;
//           scope.$on('tabChange', function(e, index, title) {
//             angular.forEach(examples, function(child) {
//               child.style.display = 'none';
//             });
//             var selected = examples[index];
//             selected.style.display = 'block';
//           });
//           scope.setTab(0);
//         }
//       };
//     }
//   };
// }])


// .directive('code2', function() {
//   return {
//     restrict: 'E',
//     terminal: true,
//     compile: function(element) {
//       var linenums = element.hasClass('linenum');// || element.parent()[0].nodeName === 'PRE';
//       var match = /lang-(\S+)/.exec(element[0].className);
//       var lang = match && match[1];
//       var html = element.html();
//       element.html(window.prettyPrintOne(html, lang, linenums));
//     }
//   };
// })

/**
<pre class="prettyprint"><code class="language-java">class Voila {
public:
  // Voila
  static const string VOILA = "Voila";

  // will not interfere with embedded <a href="#voila2">tags</a>.
}</code></pre>
*/

/**
  * Default html <rp-code> // you code </rp->code>
  *  <rp-code lang="js"> // you code </rp->code>
 */
.directive('rpCode', function() {
  return {
    restrict: 'E',
    terminal: true,
    compile: function(element) {

      var encodedStr = element.html().replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
          return '&#'+i.charCodeAt(0)+';';
      });

      element.html(encodedStr);
    }
  };
})


.directive('table', function() {
  return {
    restrict: 'E',
    link: function(scope, element, attrs) {
      if (!attrs['class']) {
        element.addClass('table table-bordered table-striped code-table');
      }
    }
  }
})

.factory('getExampleData', ['$http', '$q', function($http, $q) {
  return function(exampleFolder) {
    // Load the manifest for the example
    return $http.get(exampleFolder + '/manifest.json')
      .then(function(response) {
        return response.data;
      })
      .then(function(manifest) {
        var filePromises = [];

        angular.forEach(manifest.files, function(filename) {
          filePromises.push($http.get(exampleFolder + '/' + filename, { transformResponse: [] })
            .then(function(response) {

              // The manifests provide the production index file but Plunkr wants
              // a straight index.html
              if (filename === 'index-production.html') {
                filename = 'index.html';
              }

              return {
                name: filename,
                content: response.data
              };
            }));
        });

        return $q.all({
          manifest: manifest,
          files: $q.all(filePromises)
        });
      });
  };
}]);
