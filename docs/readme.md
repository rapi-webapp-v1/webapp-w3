
# References
- https://toddmotto.com/documenting-angular-dgeni
- https://github.com/google/code-prettify/blob/master/docs/getting_started.md


# Install
npm i dgeni dgeni-packages canonical-path lodash --save-dev

# RUN
gulp dgeni

# Start server document
cd docs/build/
lite-server
