Change Log - webap rapi w3
=============================

# [Unreleased](https://bitbucket.org/rapi3/webapp-w3/commits/all)

[Full Changelog](hhttps://bitbucket.org/rapi3/webapp-w3/branches/compare/v1.0.0%0Dmaster)

## Broken


## Bug Fixes:

## Features:

# [2.0.1](https://bitbucket.org/rapi3/webapp-w3/commits/tag/v2.0.1) (2017-09-05)
[Full Changelog](https://bitbucket.org/rapi3/webapp-w3/branches/compare/v2.0.1%0Dmaster#diff)

## Broken
- **rapi.fuse.w3** Remove module 

## Bug Fixes:
- **Upload** Fix url api upload
- **Prfile** correcao das funcoes de set profile

## Features:
- **w3ButtonUpload** new DIrective 
- **w3Currency** new DIrective 
- **w3Pusher** new Plugin 
- **w3Utils** new methods: getIds and getIdsJoin


# [1.0.4](https://bitbucket.org/rapi3/webapp-w3/commits/tag/v1.0.4) (2017-05-22)
[Full Changelog](https://bitbucket.org/rapi3/webapp-w3/branches/compare/v1.0.4%0Dmaster#diff)

## Broken
- **Auth** remove method `removeToken`
- **w3ImagePicker**  directive is deprected

## Bug Fixes:
- **w3HttInteceptor** controle da requisição 401 agora o evento `w3HttpInterceptor:loginRequired` só disparado para alguns error.code

## Features:
- **w3ImageDialog** new directive


# [1.0.3](https://bitbucket.org/rapi3/webapp-w3/commits/tag/v1.0.3) (2017-03-14)
[Full Changelog](https://bitbucket.org/rapi3/webapp-w3/branches/compare/v1.0.3%0Dmaster#diff)

## Features:
- **w3ReportDate** Create new directive w3-report-date


# [0.3.1](https://bitbucket.org/rapi3/webapp-w3/commits/tag/v0.3.1) (2017-02-28)
[Full Changelog](https://bitbucket.org/rapi3/webapp-w3/branches/compare/v0.3.1%0Dmaster#diff)

## Broken
- **w3ApiUtils** remove

## Bug Fixes:
- **w3Money** add suporte a valores negativos

## Features:
- **w3groupBy** novo filter
- **w3Phone** novo filter
- **w3Cep** novo filter
- **w3Cpf** novo filter
- **w3Cnpj** novo filter
- **helpers.js** nova função strPad
- **Date.prototype** novo methodo addDays
- **Date.prototype** novo methodo addMinutes
- **Date.prototype** novo methodo getFormatedTime
- **momment** create moment.fn.toDateBR
- **Array.prototype.sum** new prototype


# [0.3.0](https://bitbucket.org/rapi3/webapp-w3/commits/tag/v0.3.0) (2017-02-01)
[Full Changelog](https://bitbucket.org/rapi3/webapp-w3/branches/compare/v0.3.0%0Dmaster#diff)

## Broken
- **w3Activities** is deprected
- **w3Avatar** is deprected
- **w3Comments** is deprected
- **w3InputCity** is deprected
- **w3PhotoPerfil** is deprected
- **$broadcast** Rename from 'RapiAuth:loginSuccess' to 'w3Auth:loginSuccess'


## Bug Fixes:


## Features:
- **w3Config** Add suporte a plugins in w3Config |
- **w3PluginRollbar** Create w3plugin rollbar


# [0.1.3](https://bitbucket.org/rapi3/webapp-w3/commits/tag/v0.1.3) (2017-01-29)
[Full Changelog](https://bitbucket.org/rapi3/webapp-w3/branches/compare/v0.1.3%0Dmaster#diff)

## Broken
- **Auth** Change URL login to rapi/guardian

## Bug Fixes:
- **w3HttInteceptor** Show message in  http response error
- **w3HttInteceptor** Show message in  http response error forbiden
- **w3Date** Refatoring data format with momment | w3Preseter suporte tranformParamns

## Features:
- **w3Preseter** new method setTransformerParamsRequest/params (agora é possivel editar parametros de requisições GET)
- **w3Resouce** Suporte a Presenter.params in method search

# [0.1.1](https://bitbucket.org/rapi3/webapp-w3/commits/tag/v0.1.1) (2016-12-14)
[Full Changelog](https://bitbucket.org/rapi3/webapp-w3/branches/compare/v0.1.1%0Dmaster#diff)

## Bug Fixes:
- **w3Date** refatoring enToDate
- **w3Avatar** refatoring directive
- **assets** Fix url imagens plugin

## Features:
- **traducao** importados arquivos JSON de traducao do fuse para este repository
- **w3Utils** suporte a message with HTML in methods: confirmDelete e confirm


# [1.0.0](https://bitbucket.org/rapi3/webapp-w3/commits/tag/v0.0.2) (2016-12-08)
[Full Changelog](https://bitbucket.org/rapi3/webapp-w3/branches/compare/v1.0.0%0Dmaster#diff)

## Features:
- **init** nova versão separada do webapp-fuse

# [0.0.2](https://bitbucket.org/rapi3/webapp-w3/commits/tag/v0.0.2) (2016-10-23)
[Full Changelog](https://bitbucket.org/rapi3/webapp-w3/branches/compare/v0.0.2%0Dmaster#diff)

## Bug Fixes:
- **w3InputCity** diretive w3InputCity fiz set model where is NULL

## Features:
- **w3Avatar** new attribute w3-presenter
- **w3Presenter** Add paramenter index in presenter
- **w3Auth** store user e store profile on login
- **w3Resouce** suporte a w3Presenter in request e response


# [0.0.1](https://bitbucket.org/rapi3/webapp-w3/commits/tag/v0.0.1) (2016-09-09)
[Full Changelog](https://bitbucket.org/rapi3/webapp-w3/branches/compare/v0.0.1%0Dmaster#diff)
> foi copiado esse puglin de  => https://bitbucket.org/rapi3/fuse/src/95a523a721a5e9a100f5550d5b0f57f01f3ef071/resources?at=v0.1.1

## Features:
- **w3ApiUtils** new service w3ApiUtils with action searchCEP
- **w3Acitivity** create directive
- **w3Utils** create method  w3Utils.confirm
- **w3Reload** create HttpIntecptorn from config htp w3Reload (use ng-hide="$root.w3Reload['someKey']" or ng-hide="$root.loadingProgress")
- **w3Comment** create dirctive w3-comment e service RapiComment
- **w3Avatar** create dirctive w3-avatar
- **w3Config** create auth.onLoginRequired (agora deve ser passada uma closure para tratamento de login requerido)
- **w3Auth** create service
- **w3HttInteceptor** add suporte custon tokenGetter
- **toobar** logout e show name profile
- **params route** Create option state to URL external
- **w3Presenter** create service
