# install

bower install pusher-angular --save
bower install pusher --save


Inject MOdule
`rapi.w3.pusher`


```html
<!DOCTYPE html>
<html ng-app="app">
<head>
  <meta charset="utf-8">
  <title>Test</title>
  <!-- AngularJS -->
  <script src="bower_components/angular/angular.min.js"></script>
  <!-- pusher-js -->
  <script src="bower_components/pusher-js/dist/web/pusher.min.js"></script>
  <!-- pusher-angular -->
  <script src="bower_components/pusher-angular/lib/pusher-angular.min.js"></script>
  <script src="my-pusher.js"></script>
  <script type="text/javascript">
    angular
      .module('app', ['w3.pusher'])
      .controller('DemoController', DemoController);
    function DemoController(w3Pusher) {
      var channel = w3Pusher.subscribe('my-channel');
      channel.bind('App\\Events\\CheckoutStatusChangedEvent', function(data) {
        alert(data.message);
      });
      //desliga
      //channel.unbind();
    }
  </script>
</head>
<body ng-controller="DemoController">
</body>
</html>
```
