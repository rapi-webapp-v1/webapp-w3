(function () {
    'use strict';
    angular
            .module('rapi.w3.pusher', ['pusher-angular'])
//            .config(config)
            .factory('w3Pusher', w3Pusher);

    /** @ngInject */
    //mytodo: transformar em plugin
//    function config(w3ConfigProvider, RollbarProvider)
//    {
//        var pusherPlugin = w3ConfigProvider.makePlugin('pusher');
//
//        pusherPlugin.init = function (token)
//        {
//            RollbarProvider.init({
//                accessToken: token,
//                captureUncaught: true
//            });
//        };
//
//        var turorial = [
//            '###BOWER INSTAL:',
//            'bower install ng-rollbar --save',
//            '###index.config:',
//            'import module [rapi.w3.plugin.rollbar]',
//            'w3ConfigProvider.plugin(\'rollbar\').init(\'YOUR_TOKEN\')',
//            'https://github.com/tandibar/ng-rollbar'
//        ];
//
//        pusherPlugin.setTutotial(turorial);
//
//        w3ConfigProvider.addPluginProvider(pusherPlugin);
//    }

    /**
     REF: https://github.com/pusher/pusher-angular
     
     Outra package interessante
     https://github.com/doowb/angular-pusher
     
     Proprio blog do pusher falando deste a cima
     https://blog.pusher.com/making-angular-js-realtime-with-pusher/
     
     */

    /** @ngInject */
    function w3Pusher($pusher, w3Config, $window) {

        var config = w3Config.get('pusher');
        console.log('Init w3 Pushse');

        // Enable pusher logging - don't include this in production
       $window.Pusher.logToConsole = true;


        var API_KEY = 'daafb54fc3caf9e4d0db';
        var client = new Pusher(config.key, {
            //authEndpoint: "http://example.com/pusher/auth"
            cluster: config.cluster,
            encrypted: true
        });

        var pusher = $pusher(client);

        return pusher;
    }


})();
