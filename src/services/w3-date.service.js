/* global NaN */

(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .factory('w3Date', dateService);

    //helperService.$inject = [];
    //Mytodo remover o service $w3Date q so ta com um methodo extraxdate
    //Mytodo remover directive input-date ver se ainda ha necessidade de uso
    //Mytodo ver a impotese de transformar isso em $w3Date criar um padrão para service

    /** @ngInject */
    function dateService()
    {

        var service = {
            toFormatEn: toFormatEn,
            enToObj: enToObj,
            subDays: subDays,
            subMonths: subMonths,
            subMonthsInteval: subMonthsInteval,
            subYears: subYears,
            subYearsInteval: subYearsInteval,
            diffDays: diffDays,
            dateToPt: dateToPt,
            ptToDate: ptToDate,
            enToDate: enToObj,
            enToPt: enToPt,
            ptToEn: ptToEn,
            dateToEn: dateToEn
        }; 

        return service;
        ////////////////////////////

        /**
         * Transforma tanto date como datetime no formato yyyy-MM-dd HH:mm:ss to Date
         * @param {string} dt
         * @returns {undefined|Date}
         */
        function enToObj(dt)
        {
            if (!dt || angular.isString(dt) === false)
                return;

            var objDate, bits;

            bits = dt.split(/\D/);

            if (bits.length === 3) {
                objDate = new Date(bits[0], --bits[1], bits[2]);
            } else {
                objDate = new Date(bits[0], --bits[1], bits[2], bits[3], bits[4]);
            }

            return objDate;

//            if (!dt)
//                return new Date();
//            //var hora = dt.substr(11);
//
//            if (angular.isString(dt) && dt !== "") {
//                dt = dt.substr(0, 10).split('-');
//            }
//
//            //mytodo melhorar essa parte de conveter hora junto
//            // if(hora){
//            //     hora = hora.split(':');
//            //     return new Date(dt[0], (dt[1]-1), dt[2], hora[0], hora[1], hora[2]);
//            // }
//            return new Date(dt[0], (dt[1] - 1), dt[2]);
        }

        function dateToEn(date)
        {
            return date ? moment(date).format('YYYY-MM-DD') : '';
//            var m = moment(date, 'DD-MM-YYYY', true);
//            return m.isValid() ? m.toDate() : new Date(NaN);
        }

        /**
         * Tranforma um objeto Date para uma string date/datetime
         * @param {Date} dt
         * @param {boolean} full
         * @returns {string}
         */
        function toFormatEn(dt, full)
        {
            if (!dt) {
                return;
            }
            
            var format = (full === true) ? 'YYYY-MM-DD HH:mm:ss' : 'YYYY-MM-DD';
            return moment(dt).format(format);

//            var format = (full === true) ? 'yyyy-MM-dd HH:mm:ss' : 'yyyy-MM-dd';
//
//            if (angular.isDate(dt) === false) {
//                dt = Date.parse(dt);
//            }
//            dt = $filter('date')(dt, format);
//            return dt;
        }

        function subDays(objDate, nDays)
        {
            return new Date(
                    objDate.getFullYear(),
                    objDate.getMonth(),
                    objDate.getDate() - nDays
                    );
        }

        function subMonths(objDate, nMonths)
        {
            return new Date(
                    objDate.getFullYear(),
                    objDate.getMonth() - nMonths,
                    objDate.getDate()
                    );
        }

        function subMonthsInteval(objDate, nMonths)
        {
            var m = objDate.getMonth() - nMonths;
            return [
                new Date(objDate.getFullYear(), m, 1),
                new Date(objDate.getFullYear(), m + 1, 0)
            ];
        }

        function subYears(objDate, nYears)
        {
            return new Date(
                    objDate.getFullYear() - nYears,
                    objDate.getMonth(),
                    objDate.getDate()
                    );
        }

        function subYearsInteval(objDate, nYears)
        {
            var y = objDate.getFullYear() - nYears;

            return [
                new Date(y, 0, 1),
                new Date(y, 12, 0)
            ];
        }

        function diffDays(date)
        {
            var date1 = new Date(date);
            var date2 = new Date();
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            return diffDays;
        }

        /**
         * Transforma Objeto Date em string data PT
         * EX:  new Date => 25/08/2016
         * @param Date date
         * @returns string datapt
         */
        function dateToPt(date)
        {

            if(!angular.isDate(date)){
                return date;
            }

             return moment(date).format('DD/MM/YYYY');
//
//            var date1 = date;
//
//            if (!date1) {
//                return;
//            } else {
//
//                //return date ? '*' + moment(date).format('DD/MM/YYYY') : '';
//                //date1 = date1.addDays(1);
//                date1 = $filter('date')(date1, 'dd/MM/yyyy');
//                //date1 = date1.split("-").join("/");
//
//                return date1;
//            }

        }

        /**
         * Transforma string data PT  em Objeto Date
         * EX:  25/08/2016 => new Date
         * @param string input
         * @returns Date
         */
        function ptToDate(date)
        {
            var date1 = date;

            if (!date1) {
                return;
            } else {
                date1 = date1.split("/");
                date1 = new Date(date1[2], date1[1] - 1, date1[0]);

                return date1;
            }
        }

        /**
         * Transforma data EN em PT
         * EX: 2016-08-25 => 25/08/2016
         * @param string input
         * @returns string
         */
        function enToPt(input)
        {

            if (!input) {
                return;
            } else {
                var d = input.split("-");
                input = [d[2], d[1], d[0]].join('/');

                return input;
            }
        }

        /**
         * Transforma data PT em EN
         * EX: 25/08/2016 => 2016-08-25
         * @param string input
         * @returns string
         */
        function ptToEn(input)
        {

            if (!input) {
                return;
            } else {
                var d = input.split("/");
                input = [d[2], d[1], d[0]].join('-');

                return input;
            }
        }

    }


})();
