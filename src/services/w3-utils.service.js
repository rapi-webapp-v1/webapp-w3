(function ()
{
    'use strict';

    angular
            .module('rapi.w3')
            .factory('w3Utils', w3Utils);

    /** @ngInject */
    function w3Utils($document, $mdDialog, $state, w3Config)
    {

        var configs = {
            baseUrl: 'http://localhost:8000',
            env: 'localhost'
        };

        var service = {
            getUrl: getUrl,
            urlApi: urlApi,
            urlApiInclude: urlApiInclude,
            getLocale: getLocale,
            alert: alert,
            confirmDelete: confirmDelete,
            confirm: confirm,
            openPopUp: openPopUp,
            makeUrlApp: makeUrlApp,
            getIds : getIds,
            getIdsJoin : getIdsJoin
        };

        activate();
        return service;

        //////////

        function activate()
        {
            angular.extend(configs, w3Config.get('host'));
        }

        function getBaseUrl()
        {
            return configs.baseUrl;
        }

        /**
         * ex url: /marcas/{id}/action/{action}
         * cria-se um objeto {id:5, action: 'delete'}
         * resultado: http://localhost:8000/api/v1/marcas/5/action/delete
         *
         * Se passar um integer no lugar do obejto ele procura automaticamente o {id}
         * ex url: /marcas/{id}/teste
         * cria-se um id 5
         * resultado: http://localhost:8000/api/v1/marcas/5/teste
         */
        function urlApi(url, params)
        {
            url = configs.baseUrlApi + url;

            if (angular.isObject(params)) {
                angular.forEach(params, function (v, k) {
                    url = url.replace('{' + k + '}', v);
                });
            } else if (params)
            {
                url = url.replace('{id}', params);
            }

            return getUrl(url);
        }

        /**
         *  apenas adciona ?include ao final da url para evitar de ter que ficar criando toda aquele array data{params: {include: .. } }
         */
        function urlApiInclude(url, params, include)
        {
            url = urlApi(url, params);

            if (include) {
                url += "?include=" + include;
            }

            return url;
        }

        function getUrl(url)
        {
            url = url || '';
            return getBaseUrl() + url;
        }

        function getLocale()
        {
            return 'pt-br';
            //return 'es';//pt-br';
        }

        function alert(title, message, labelBtn)
        {
            labelBtn = labelBtn || 'OK';

            var dialog = $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title(title)
                    .ariaLabel(title)
                    .parent(angular.element($document.body))
                    .ok(labelBtn);

            if (message)
                dialog.textContent(message);

            return $mdDialog.show(dialog);
        }


        function confirmDelete(title, message, ev)
        {

            var templateModal = [
                '<md-dialog aria-label="Excluir conta" class="dialog-md mv-modal-default">',
                '<md-toolbar class="md-warn">',
                '<div class="md-toolbar-tools">',
                '<h2>{{title}}</h2>',
                '    <span flex></span>',
                '    <md-button class="md-icon-button" ng-click="cancel()" aria-label="fechar dialog">',
                '<i class="ion ion-close-round" style="color:#fff"></i>',
                '</md-button>',
                '</div>',
                '</md-toolbar>',
                '<md-dialog-content style="max-width:800px;max-height:810px; ">',
                '   <div>',
                '       <h3 ng-bind-html="message"></h3>',
                '   </div>',
                '</md-dialog-content>',
                '<md-dialog-actions>',
                '   <span flex></span>',
                '   <md-button ng-click="cancel()" translate="COMMON.BTN_CANCEL">Cancelar</md-button>',
                '   <span flex="5"></span>',
                '   <md-button ng-click="excluir($event)" class="md-raised md-warn" translate="COMMON.BTN_EXCLUIR"> Excluir</md-button>',
                '</md-dialog-actions>',
                '</md-dialog>'
            ].join('');

            function ModalConfirmeDeleteCtlr($scope, $mdDialog, title, message)
            {

                $scope.title = title || "Confirmação de exclusão";
                $scope.message = (message === null) ? "Realmente deseja excluir esse registro?" : message;

                $scope.cancel = cancel;
                $scope.excluir = excluir;

                //----------Functions------------

                function cancel() {
                    $mdDialog.cancel();
                }

                function excluir() {
                    $mdDialog.hide(true);
                }

            }

            return $mdDialog.show({
                controller: ModalConfirmeDeleteCtlr,
                template: templateModal,
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                resolve: {
                    title: function () {
                        return title;
                    },
                    message: function () {
                        return message;
                    }
                }
            });

        }


        function confirm(title, message, ev)
        {

            var templateModal = [
                '<md-dialog aria-label="Confirmação" class="dialog-md mv-modal-default">',
                '<md-toolbar class="md-accent">',
                '<div class="md-toolbar-tools">',
                '<h2>{{title}}</h2>',
                '    <span flex></span>',
                '    <md-button class="md-icon-button" ng-click="cancel()" aria-label="fechar dialog">',
                '<i class="ion ion-close-round" style="color:#fff"></i>',
                '</md-button>',
                '</div>',
                '</md-toolbar>',
                '<md-dialog-content style="max-width:800px;max-height:810px; ">',
                '   <div>',
                '       <h3 ng-bind-html="message"></h3>',
                '   </div>',
                '</md-dialog-content>',
                '<md-dialog-actions>',
                '   <span flex></span>',
                '   <md-button ng-click="cancel()" translate="COMMON.BTN_CANCEL">Cancelar</md-button>',
                '   <span flex="5"></span>',
                '   <md-button ng-click="confirmar($event)" class="md-raised md-accent" translate="COMMON.BTN_CONFIRM">Confirmar</md-button>',
                '</md-dialog-actions>',
                '</md-dialog>'
            ].join('');

            function ModalConfirmeCtlr($scope, $mdDialog, title, message)
            {

                $scope.title = title || "Confirmação";
                $scope.message = (message === null) ? "Deseja executar esta ação?" : message;

                $scope.cancel = cancel;
                $scope.confirmar = confirmar;

                //----------Functions------------

                function cancel() {
                    $mdDialog.cancel();
                }

                function confirmar() {
                    $mdDialog.hide(true);
                }

            }

            return $mdDialog.show({
                controller: ModalConfirmeCtlr,
                template: templateModal,
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                resolve: {
                    title: function () {
                        return title;
                    },
                    message: function () {
                        return message;
                    }
                }
            });

        }

        function openPopUp(stateOrName, params, w, h)
        {
            var url = $state.href(stateOrName, params);
            var options = [];

            w = w || 740;
            h = h || 600;


            options.push('height=' + h);
            options.push('width=' + w);

            return window.open(url, "webapp", options.join(','));
        }

        function makeUrlApp(path)
        {
            var configHost = w3Config.get('host');
           

            if (configHost.env === 'local') {
                return [configHost.appUrl, path].join('/');
            }

            return [configHost.appUrl, configHost.baseModule, path].join('/');
        }
        
         /*
         * Responsável por pegar os is dos chips e retornar em uma string separapor |
         */
        function getIds(colection)
        {
            if (!angular.isArray(colection))
                return null;

            return colection.map(function (row) {
                return row.id;
            });
        }
        
         /*
         * Responsável por pegar os is dos chips e retornar em uma string separapor |
         */
        function getIdsJoin(colection)
        {
            if (!angular.isArray(colection))
                return null;

            return getIds(colection).join('|');
        }

        
        /**
         * //mytodo criar um service so para manipular array e objetos o fuse ja tem alguns recursos
         * @param {type} obj
         * @returns {undefined}
         */
//        function cleanAttributes(obj)
//        {
//            for (var i in obj) {
//                if (test[i] === null || test[i] === undefined) {
//                    delete test[i];
//                }
//            }
//        }

    
    }
}());
