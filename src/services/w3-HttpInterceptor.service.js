(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .provider('w3HttpInterceptor', w3HttpInterceptor);


    function w3HttpInterceptor() {

        var toastr;

        //quando o token é enviado via URL e nao pelo header, ex: ?access_token
        this.urlParam = null;

        //nome da key que vai ser utilizada no header
        this.authHeader = 'Authorization';

        //prefix do valor to token passado no header
        this.authPrefix = 'Bearer ';

        //tokenGetter: w3Auth.getToken,
        //função que retorna o token
        this.tokenGetter = null;

        //Quando configurado sera inserido o token apenas nas request que iniciam com esse prefixo
        this.urlPrefix = 'api/v1';

        //Mescla os objeto com o request.header
        this.prependHeaders = null;// Ex: {'FOO':'bar'}

        var config = this;

        this.$get = ['$q', '$rootScope', '$log', '$injector',
            function ($q, $rootScope, $log, $injector) {

                $rootScope.w3Reload = {};

                return {
                    request: function (request) {

                        if (request.w3Reload) {
                            $rootScope.w3Reload[ request.w3Reload ] = true;
                        }

                        //Casa tenha paramentro na request dizendo para ingnorar Authorization, ele tmb não add
                        if (request.skipAuthorization) {
                            return request;
                        }

                        //Caso a URL não seja de api nem add o token
                        if (request.url.indexOf(config.urlPrefix) < 0) {
                            return request;
                        }

                        $rootScope.loadingProgress = true;

                        if (config.urlParam) {
                            request.params = request.params || {};
                            // Already has the token in the url itself
                            if (request.params[config.urlParam]) {
                                return request;
                            }
                        } else {
                            request.headers = request.headers || {};

                            if(angular.isObject(config.prependHeaders)){
                              request.headers = angular.extend(request.headers, config.prependHeaders);
                            }

                            // Already has an Authorization header
                            if (request.headers[config.authHeader]) {
                                return request;
                            }
                        }

                        //Resolve tokenGetter
                        //if(config.tokenGetter === null){
                        //  config.tokenGetter = $injector.get('w3Auth').getToken;
                        //}

                        var tokenPromise = $q.when($injector.invoke(config.tokenGetter, this, {
                            config: request
                        }));

                        return tokenPromise.then(function (token) {
                            if (token) {
                                if (config.urlParam) {
                                    request.params[config.urlParam] = token;
                                } else {
                                    request.headers[config.authHeader] = config.authPrefix + token;
                                }
                            }
                            return request;
                        });
                    },
                    'response': function (response) {
                        responseLoading(response);
                        responseMensage(response.data);
                        return response || $q.when(response);
                    },
                    'responseError': function (rejection) {

                        var msg;

                        responseLoading(rejection);

                        if (rejection.status === 400) {
                            //Erro de validacao
                            responseMessageValidation(rejection.data);
                        } else if (rejection.status === 403) {
                            //Usuario não possui essa permissão CAM/ROLE
                            msg = (rejection.data.error && rejection.data.error.message) ? rejection.data.error.message : 'Seu usuário não possui acesso!';
                            responseWarn(msg);
                            $rootScope.$emit('w3HttpInterceptor:permissionRequired', rejection);
                        } else if (rejection.status === 404) {
                            //Não encontrado
                            $rootScope.$emit('w3HttpInterceptor:notFound', rejection);
                            responseMessageNotFound(rejection.data);
                        } else if (rejection.status === 401) {
                            //User não autorizado | User não logado
                            msg = (rejection.data.error && rejection.data.error.message) ? rejection.data.error.message : 'Favor efetuar login!';
                            responseInfo(msg);
                            checkLoginRequired(rejection);
                        } else if (rejection.status) {
                            //alert(angular.toJson(rejection))
                            $log.error(rejection);

                            if (rejection.data.error && rejection.data.error.message)
                                responseError(rejection.data.error.message);

                        }

                        return $q.reject(rejection);
                    }
                };

                function checkLoginRequired(rejection)
                {
                    var codes = ['token_not_provided', 'UNAUTHORIZED'];
                    var responseCode = (rejection.data.error && rejection.data.error.code) ? rejection.data.error.code : null;

                    console.warn('HTTP INTERCEPTOR 401', rejection);

                    if(codes.indexOf(responseCode) !== false){
                        $rootScope.$emit('w3HttpInterceptor:loginRequired', rejection);
                    }
                }

                //-----------------------------------------
                /**
                 * Verifica o retorno da requisição e alta os status de loadingProgress
                 * @param response is response|rejection
                 */
                function responseLoading(response)
                {
                    if (response.config && 'w3Reload' in response.config) {                        
                        delete $rootScope.w3Reload[ response.config.w3Reload ];
                    }

                    //Caso a URL seja de uma api ele para reload
                    if (response.config.url.indexOf(config.urlPrefix) !== -1) {
                        $rootScope.loadingProgress = false;
                    }
                }

                function getToaster() {
                    if (!toastr) {
                        toastr = $injector.get("toastr");
                    }
                    return toastr;
                }

                function responseError(message) {
                    getToaster().error(message);
                }

                function responseInfo(message) {
                    getToaster().info(message);
                }

                function responseWarn(message) {
                    getToaster().warning(message);
                }

                function responseMensage(data) {

                    if (data.message) {

                        var type = (data.status === 'error' || data.status === 'success') ? data.status : 'note';
                        var error = null;

                        if (data.errors) {
                            error = '<ul>';
                            angular.forEach(data.errors, function (item) {
                                error += '<li>' + item + '</li>';
                            });
                            error += '</ul>';
                        }

                        if (type === 'success') {
                            getToaster().success(error, data.message);
                        } else if (type === 'error') {
                            getToaster().error(error, data.message);
                        } else {
                            getToaster().info(error, data.message);
                        }

                    }


                }

                function responseMessageNotFound(data) {
                    if (data.status === "error" && data.error.message) {
                        $log.info(data.error.message);
                        getToaster().error(data.error.message);
                    }
                }

                function responseMessageValidation(data) {

                    if (!data.error)
                        return false;

                    //var type = (data.status === 'error' || data.status === 'success') ? data.status : 'note';
                    var error = null;

                    if (data.error.validation) {
                        error = '<ul>';
                        angular.forEach(data.error.validation, function (msg) {
                            error += '<li>' + msg + '</li>';
                        });

                        error += '</ul>';
                    }

                    getToaster().error(error, data.error.message);

                }

            }];


    }

})();
