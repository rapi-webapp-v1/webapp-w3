(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .factory('w3Auth', w3AuthService);

    /**
     * //ToDO
     *
     * $auth.signup(user, [options])
     * $auth.logiSocialite(name, [userData]) //redes sociais
     */

    /** @ngInject */
    function w3AuthService($http, $q, $rootScope, $log, w3Utils, w3Local, w3JWT)
    {
        var JWT;
        var service = {
            login: login,
            loginFromToken: loginFromToken,
            profile: profile,
            user: user,
            logout: logout,
            check: check,
            guest: guest,
            getToken: getToken,
            token: getTokenWhen, 
            getPayload: getPayload,
            setToken: setToken
        };

        return service;
        ////////////////

        function login(credentials)
        {
            var deferred = $q.defer();

            var config = {
                skipAuthorization: true
            };

    

            $http.post(w3Utils.urlApi('/rapi/guardian/auth/token/issue'), credentials, config).then(
                    function (result) {

                        if (result.data.status === 'success') {
                            _handlerOnSuccess(result.data.data);
                            deferred.resolve(result.data.data);
                        }

                        deferred.reject(result);

                    },
                    function (error) {
                        deferred.reject(error);
                    }
            );

            return deferred.promise;
        }

        function loginFromToken(token)
        {
            var deferred = $q.defer();

            $http({
                url: w3Utils.urlApi('/rapi/guardian/me'), //Profile
                skipAuthorization: true,
                method: 'GET',
                params: {
                    'include': 'user'
                },
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }).then(
                    function (result) {

                        if (result.data.data.user_id >= 1) {
                            _handlerOnSuccess(result.data.data);
                            deferred.resolve(result.data.data);
                        }

                        deferred.reject(result);

                    },
                    function (error) {
                        deferred.reject(error);
                    }
            );

            return deferred.promise;
        }

        function _handlerOnSuccess(result)
        {
            setToken(result.token);
            
            setProfile(result);
            w3Local.set('rapi.auth.user', result);
            $rootScope.$broadcast('w3Auth:loginSuccess', result);
        }

        function logout()
        {
            return $http.post(w3Utils.urlApi('/rapi/guardian/auth/token/revoke')).then(function () {
                setToken(null);
                setProfile(null);
                w3Local.unset('rapi.auth.user');
                $rootScope.$emit('w3Auth:logout');
            });
        }

        function check()
        {
            var token = getJWT();

            if (!token) {//not token
                return false;
            }

            if (w3JWT.isTokenExpired(token)) {//token expired
                return false;
            }

            return true;
        }

        function guest()
        {
            return (check()) ? false : true;
        }

        function getToken()
        {
            var token = getJWT();

            if (token) {

                if (w3JWT.isTokenExpired(token)) {

                    return refreshToken(token);

                } else {
                    return token;
                }

            }
        }

        function getTokenWhen()
        {
            return $q.when(getToken());
        }

        function refreshToken(token)
        {
            return $http({
                url: w3Utils.urlApi('/rapi/guardian/auth/token/refresh'),
                skipAuthorization: true,
                method: 'POST',
                headers: {Authorization: 'Bearer ' + token}
            }).then(function (response) {
                $log.info('Refresh Token');
                setToken(response.data.data.token);
                return response.data.data.token;
            }, function (err) {
                setToken(null);
                $log.eror('RefreshToken:error', err);
                return false;
            });
        }

        function getJWT()
        {
            if (!JWT) {
                JWT = w3Local.get('rapi.auth.token');
            }

            return JWT;
        }

        function getPayload()
        {
            var token = getJWT();
            return w3JWT.decodeToken(token);
        }

        function setToken(token)
        {
            JWT = token;
            w3Local.set('rapi.auth.token', token);
            $log.debug('w3Auth.setToken', token);
        }

        function profile()
        {
            return w3Local.get('rapi.auth.profile');
        }

        function setProfile(profile)
        {
            w3Local.set('rapi.auth.profile', profile);
            $rootScope.$broadcast('w3Auth:onChangeProfile', profile);
        }

        function user()
        {
            return w3Local.get('rapi.auth.user');
        }

    }
})();
