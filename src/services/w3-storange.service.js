(function() {
  'use strict';

/**
$log.info( w3Local.set('tdd', 1234) === w3Local.set('tdd', 1234) );
$log.info( w3Local.set('tdd', 1234) === w3Local.set('tdd2', 1234) );
$log.info( w3Local.get('ts', 1234) === 1234 );
w3Local.set('ts', {nome:'valmir'})
$log.info( w3Local.get('ts').nome === 'valmir' );
*/

  angular
    .module('rapi.w3')
    .service('w3Local', local)
    .service('w3Session', session);

  function storange(drive) {

    /* jshint validthis: true */
    var vm = this;
    vm.drive = drive;
    vm.set = set;
    vm.get = get;
    vm.unset = unset;
    vm.reset = reset;
    
    //Mytodo create set namespace e unsetNamespace

    function set(key, value) {
      value = angular.toJson(value);
      this.drive.setItem(key, value);
    }

    function get(key, defaul) {

      var item = this.drive.getItem(key);

      if (!item || item === 'null') {
        return defaul;
      }

      try {
        return angular.fromJson(item);
      } catch (e) {
        return defaul;
      }

    }

    function unset(key) {
      this.drive.removeItem(key);
    }

    function reset() {
      this.drive.clear();
    }

  }

  function local() {
    return new storange(window.localStorage);
  }

  function session() {
    return new storange(window.sessionStorage);
  }



})();
