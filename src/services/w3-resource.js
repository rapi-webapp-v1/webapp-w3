(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .service('w3Resource', w3Resource);

    /** @ngInject */
    function w3Resource($http, w3Utils) {

        this.make = function (urlApi, presenter) {
            return resourceModelo($http, w3Utils, urlApi, presenter);
        };

        this.extend = function (urlApi, service, presenter) {
            var resource = this.make(urlApi, presenter);
            angular.extend(resource, service);
            return resource;
        };

    }

    function resourceModelo($http, w3Utils, urlApi, presenter) {

        function Service() {
            this.page = 1;
            this.perPage = 15;
            this.name = 'w3Resource';
            this.url = urlApi;
            this.pagination = null;
            this.transformerRequest = null;
            this.transformerResponse = null;
        }


        /** retorna os primeiros 15 items itens da rota + includes */
        Service.prototype.get = function (includes) {
            return this.search({
                'include': includes,
                'paginate': this.perPage
            });
        };
        
        /** retorna todos os itens da rota + includes */
        Service.prototype.all = function (includes) {
            return this.search({
                'include': includes,
                'take': -1
            });
        };

        /**
         * Registra a pagina
         * @param int newPage
         */
        Service.prototype.setPage = function (newPage) {
            this.page = newPage;
        };

        /** retorna os itens da rota  filtrados por params*/
        Service.prototype.search = function (params) {

            var vm, data;

            vm = this;
            
            var dataParams = (presenter)? presenter.params(params) : params;
            dataParams.page = this.page;
            
            data = {
                w3Reload: vm.name + '.search',
                params: dataParams
            };

            return $http.get(w3Utils.urlApi(this.url), data).then(function (result) {
                vm.pagination = (result.data.meta) ? result.data.meta.pagination : null;
                return (presenter)? presenter.collection(result.data.data) : result.data.data;
            });
        };

        /**
         * Busca um registro pelo ID
         * @param id integer codifo indentificador
         * @param includeEmbed string
         * @return $q
         * */
        Service.prototype.find = function (id, includeEmbed) {

            var data = {
                w3Reload: this.name + '.find',
                params: {
                    'include': includeEmbed
                }
            };

            return $http.get(w3Utils.urlApi(this.url + '/{id}', {
                id: id
            }), data).then(function (result) {
                return (presenter)? presenter.item(result.data.data) : result.data.data;
            });

        };

        /** salva data */
        Service.prototype.save = function (data) {

            var config = {
                'w3Reload': this.name + '.save'
            };
            
            var dataRequest = (presenter)? presenter.request(data) : data;
            
            return $http.post(w3Utils.urlApi(this.url), dataRequest, config).then(
                    function (result) {

                        if (result.status === 201) {
                            var transformer = (presenter)? presenter.item(result.data.data) : result.data.data;
                            angular.extend(data, transformer);
                            return data;
                        }

                        return false;
                    });
        };

        /** edita informações do idem id */
        Service.prototype.update = function (id, data) {

            var config = {
                'w3Reload': this.name + '.update'
            };
            
            var dataRequest = (presenter)? presenter.request(data) : data;

            return $http.put(w3Utils.urlApi(this.url + '/{id}', {
                id: id
            }), dataRequest, config).then(function (result) {

                if (result.data.status === 'success') {
                    var transformer = (presenter)? presenter.item(result.data.data) : result.data.data;
                    angular.extend(data, transformer);
                    return data;
                }

                return false;
            });
        };


        Service.prototype.remove = function (id) {

            var config = {
                'w3Reload': this.name + '.remove'
            };

            return $http.delete(w3Utils.urlApi(this.url + '/{id}', {
                id: id
            }), config).then(function (result) {
                return (result.data.status === 'success') ? true : false;
            });
        };

        return new Service();
    }



})();


/***
 * //mytodo implemets  w3Presenter in w3resource
 */
//function modeloResouce2(w3Date, w3Presenter, w3Resource) {
//
//        var Presenter = w3Presenter.make(
//                transformerResponse,
//                transformerRequest
//                );
//
//        var service = {
//            otherMethd: otherMethd,
//        };
//
//        return w3Resource.extend(service, Presenter);
//
//        function transformerResponse(response) {
//            response.nome = response.nome.toUpperCase();
//            response.data_nasc = w3Date.enToObj(response.data_nasc);
//            return response;
//        }
//
//        function transformerRequest(request) {
//            request.data_nasc = w3Date.toFormatEn(request.data_nasc);
//            return request;
//        }
//
//        function otherMethd() {
//            return $http.get(w3Utils.urlApi('/painel/me'), data).then(function (result) {
//                return Presenter.item(result.data.data);
//            });
//        }
//
//    }
//
//    function modeloResouce3(w3Date, w3Presenter, w3Resource) {
//
//        var Presenter = w3Presenter.make(
//                transformerResponse,
//                transformerRequest
//                );
//
//        return w3Resource.get(Presenter);
//
//        function transformerResponse(response) {
//            response.nome = response.nome.toUpperCase();
//            response.data_nasc = w3Date.enToObj(response.data_nasc);
//            return response;
//        }
//
//        function transformerRequest(request) {
//            request.data_nasc = w3Date.toFormatEn(request.data_nasc);
//            return request;
//        }
//
//    }
