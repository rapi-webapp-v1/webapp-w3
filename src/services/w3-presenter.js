(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .service('w3Presenter', makeService);

    /** @ngInject */
    function makeService() {

        this.make = function (response, request, params) {
            return new Service(response, request, params);
        };

    }

    function Service(response, request, params) {
        this.transformerRequest = request;
        this.transformerResponse = response;
        this.transformerParams = params;
    }

    Service.prototype.setTransformerRequest = function (func) {
        this.transformerRequest = func;
    };

    Service.prototype.setTransformerResponseRequest = function (func) {
        this.transformerResponse = func;
    };
    
    Service.prototype.setTransformerParamsRequest = function (func) {
        this.transformerParams = func;
    };

    Service.prototype.collection = function (results) {
        var presenter = this.transformerResponse;    
        var vm = this;
        return results.map(function (result, index) {
            return vm.presenter(result, presenter, index);
        });
    };
    
    Service.prototype.item = function (result) {
        return this.presenter(result, this.transformerResponse);
    };
    
    Service.prototype.request = function (result) {
        return this.presenter(result, this.transformerRequest);
    };
    
    Service.prototype.params = function (result) {
        return this.presenter(result, this.transformerParams);
    };
    
    Service.prototype.presenter = function(result, presenter, index){
        
        if(angular.isFunction(presenter) === false){
            return result;
        }
        
        var data = angular.copy(result);
        return presenter(data, index);  
    };


})();

/***
 * Modelo de uso
 * 
 var Presenter = w3Presenter.make(
        function (result)
        {
            console.log(222, result);
            result.nome = 2;
            return result;
        },
        function (result)
        {
            console.log(222, result);
            result.ok = 2;
            return result;
        }
);

//service

function save(data) {
    var newData = Presenter.request(data);

    return $http.post(w3Utils.urlApi(route), newData).then(function (result) {
        return (result.status === 201) ? Presenter.item(result.data.data) : false;
    });
}

function search(params) {

    var data = {
        params: params
    };

    return $http.get(w3Utils.urlApi(route), data).then(function (result) {
        return Presenter.collection(result.data.data);
    });

}

 */