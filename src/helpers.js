/* global moment */

function strPad(value, length)
{
    length = length || 2;
    var pad = "000000";
    return (pad + value.toString()).slice(-length);
}

Date.prototype.addDays = function (days)
{
    this.setDate(this.getDate() + days);
    return this;
};

Date.prototype.addMinutes = function (min)
{
    min = min || 0;
    min = this.getMinutes() + min;
    this.setMinutes(min);
    return this;
};

Date.prototype.getFormatedTime = function ()
{
    return [
        strPad(this.getHours( )),
        strPad(this.getMinutes())
    ].join(':');
};


moment.fn.toDateBR = function () {
    return new Date(this.year(), this.month(), this.date(), this.hour(), this.minute(), this.second(), this.millisecond());
};


Array.prototype.sum = function (prop) {
    var total = 0;
    for (var i = 0, _len = this.length; i < _len; i++) {
        total += this[i][prop];
    }
    return total;
};

/**
 * Transform tring to slug
 * @param {string} 
 * @returns {slug}
 */
function slugify(text)
{
    return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
}