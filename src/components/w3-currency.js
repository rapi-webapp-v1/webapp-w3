(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .directive('w3Currency', w3CurrencyDirective);

    /**
     * use <input w3-currency ng-model='vm.quantidade' limit="2" prefix="" /> default currency
     * prefix is optional />
     * @ngInject */
    function w3CurrencyDirective($filter) {

        return {
            require: '?ngModel',
            scope: {
                limit: '=limit',
                prefix: '=prefix'
            },
            link: function (scope, elem, attrs, ctrl) {
                
                var limite = scope.limit ? scope.limit : '2';
                var prefixo = scope.prefix ? scope.prefix : ''; // exemple: R$, USD
                
                if (!ctrl)
                    return;
                
                var format = attrs.w3Money || 'currency';

                ctrl.$formatters.unshift(function () {

                    return $filter(format)(ctrl.$modelValue, prefixo, limite);
                });

                ctrl.$parsers.unshift(function () {//viewValue

                        elem.priceFormat({
                            allowNegative: true,
                            centsLimit: this.limite,
                            prefix: prefixo,
                            centsSeparator: ',',
                            thousandsSeparator: '.'
                        });
                    
                    return parseFloat(elem[0].value.replace(/\./g, '').replace(',', '.')) || 0;
                });
            }
        };
    }

})();
