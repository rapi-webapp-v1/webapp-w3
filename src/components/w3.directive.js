(function ()
{
    'use strict';

    angular
            .module('rapi.w3')
            .directive('w3Enter', w3EnterDirective);

    /** @ngInject */
    function w3EnterDirective() {

        return function (scope, element, attrs) {
            element.bind("keydown", function (event) {

                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.w3Enter);
                    });

                    event.preventDefault();
                }
            });
        };

    }

})();
