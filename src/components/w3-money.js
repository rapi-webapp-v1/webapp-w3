(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .directive('w3Money', w3MoneyDirective);

    /**
     * use <input w3-money ng-model='vm.quantidade' /> default currency
     * use <input w3-money="[number|currency]" ng-model='vm.price' />
     * @ngInject */
    function w3MoneyDirective($filter) {

        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl)
                    return;

                var format = attrs.w3Money || 'currency';

                ctrl.$formatters.unshift(function () {

                    return $filter(format)(ctrl.$modelValue);
                });

                ctrl.$parsers.unshift(function () {//viewValue

                    if (format === 'number') {

                        elem.priceFormat({
                            allowNegative: true,
                            centsLimit: 0,
                            prefix: '',
                            centsSeparator: ',',
                            thousandsSeparator: '.'
                        });

                    } else {

                        elem.priceFormat({
                            allowNegative: true,
                            prefix: '',
                            centsSeparator: ',',
                            thousandsSeparator: '.'
                        });

                    }
                    return parseFloat(elem[0].value.replace(/\./g, '').replace(',', '.')) || 0;
                });
            }
        };
    }

})();
