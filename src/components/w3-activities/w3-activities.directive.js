(function () {
    'use strict';

    //mytodo destruir essa diretiva, pois já foi migrada pro fuse para rfActivities

    angular
            .module('rapi.w3')
            .directive('w3Activities', activitiesDirective)
            .controller('w3ActivitiesController', activitiesController);


    /**
     * USAGE:
     * <w3-activities w3-token="vm.row.token_morph.token"></w3-activities>
     *
     * @constructor
     */
    function activitiesDirective() {
        return {
            restrict: 'E',
            scope: {
                token: "=w3Token"
            },
            controller: 'w3ActivitiesController as vm',
            templateUrl: '/app/external/w3/components/w3-activities/activities.tmpl.html'
        };
    }

    /** @ngInject */
    function activitiesController($scope, $http, $log, w3Utils)
    {
        var vm = this;
        vm.activities = [];

        /*  methods */



        /////////////////////////////////////////////
        $scope.$watch('token', activate);

        function activate()
        {

            $log.warn('Deprecation warning: w3Activities is deprecated, use <rf-activities rf-token></rf-activities>');

            if ($scope.token) {
                loadLogs();
            }

            $scope.$on('w3Activities.reload', function(){// este callback envia um paramentro event
                //event.preventDefault();//myFeature implements pusher socket io
                loadLogs();
            });
        }

        function loadLogs() {

            var data = {
                params: {
                    'token_morph': $scope.token,
                    'include': 'causer',
                    'sort': '-created_at',
                    'take': -1 //mytodo paginar result (colocar btn de ler mais) page++
                }
            };

            var url = w3Utils.urlApi('/rapi/activities');

            $http.get(url, data).then(function (result) {
                vm.activities = result.data.data;
            });
        }

    }


})();
