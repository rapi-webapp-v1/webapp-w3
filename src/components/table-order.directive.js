(function ()
{
    'use strict';

    angular
            .module('rapi.w3')
            .directive('w3PageTable', w3PageTableDirective)
            .directive('w3OrderTable', w3OrderTableDirective);


    /** @ngInject */
    function w3PageTableDirective()
    {
        return {
            restrict: 'E',
            scope: {
                pagination: '=w3Pagination',
                setPage: '&w3SetPage'
            },
            template: [  
                '{{pagination|w3Paginate}}',
                '<md-button ng-click="prevpage()" ng-disabled="page <= 1">',
                '   <md-icon md-font-icon="icon-chevron-left" class="icon s26"></md-icon>',
                '    {{\'RAPI.PAGINATE_PREV\'|translate}}',
                '</md-button>',
                '<md-button ng-click="nextpage()" ng-disabled="page >= pagination.total_pages">',
                '   <span translate="RAPI.PAGINATE_NEXT"> Próximo </span>',
                '    <md-icon md-font-icon="icon-chevron-right" class="icon s26"></md-icon>',
                '</md-button>'].join(''),
            link: function (scope)
            {
                scope.nextpage = nextpage;
                scope.prevpage = prevpage;
                scope.page     = scope.pagination.current_page || 1;

                function setPage(p) {

                    if (p < 1 || p > scope.pagination.total_pages)
                        return;

                    scope.$evalAsync(function ()
                    {
                        scope.page = p;
                        scope.setPage({'$page':p});
                    });
                }

                function prevpage() {
                    setPage(scope.pagination.current_page - 1);
                }

                function nextpage() {
                    setPage(scope.pagination.current_page + 1);
                }
            }
        };
    }

    /** @ngInject */
    function w3OrderTableDirective()
    {
        return {
            restrict: 'A',
            scope: {
                params: '=w3OrderTable',
                onChange: '&w3Change'
            },
            link: function (scope, iElement)
            {

                activate();

                function getOrder()
                {
                    var sort = [];

                    iElement.find('thead>tr>th').each(function (k, el) {
                        var $el = angular.element(el),
                                key = $el.data('order');

                        if ($el.hasClass('sorting_desc')) {
                            sort.push("-" + key);
                        } else if ($el.hasClass('sorting_asc')) {
                            sort.push(key);
                        }


                    });

                    return sort.join(',');
                }

                /**
                 * Toggle Order
                 */
                function toggleOrder()
                {                    
                    var $el = $(this);

                    if ($el.hasClass('sorting_desc')) {
                        $el.removeClass('sorting_desc');
                    } else if ($el.hasClass('sorting_asc')) {
                        $el.removeClass('sorting_asc');
                        $el.addClass('sorting_desc');
                    } else {
                        $el.addClass('sorting_asc');
                        $el.removeClass('sorting_desc');
                    }

                    scope.$evalAsync(function ()
                    {
                        scope.params.sort = getOrder();
                        scope.onChange();
                    });

                }

                function activate()
                {
                    var sort = scope.params.sort.split(',') || [];

                    iElement.find('thead>tr>th').each(function (k, el) {
                        var $el = angular.element(el),
                                key = $el.data('order');

                        if ($el.data('order')) {
                            $el.addClass('sorting');
                            $el.on('click', toggleOrder);
                        }

                        if (sort.indexOf(key) >= 0) {
                            $el.addClass('sorting_asc');
                        } else if (sort.indexOf("-" + key) >= 0) {
                            $el.addClass('sorting_desc');
                        }
                    });
                }

            }
        };
    }
})();
