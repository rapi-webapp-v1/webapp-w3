(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .directive('w3ImageDialog', w3ImageDialogDirective)
            .controller('DialogController', DialogController)
            .controller('w3ImageDialogController', w3ImageDialogController);

    /**
     * use: <w3-image-dialog path="vm.row.logo" path-src="vm.row.logo_src" gallery="nomeGaleria" title="Selecione uma foto" config="configUpload"></w3-image-dialog>
     *
     * Modelo de configuração
     *  config = {
     *      url: w3Utils.urlApi('/uploads'),
     *      params: {
     *          'owner_type': 'LDM\\Casal\\Casal',
     *          'owner_id': null
     *      }
     *  }
     *
     @ngInject  */
    function w3ImageDialogDirective()
    {
        return {
            restrict: 'E',
            templateUrl: '/app/external/w3/components/w3-uploud/button-upload.html',
            controller: 'w3ImageDialogController as vm',
            bindToController: true,
            scope: {
                'path': '=',
                'pathSrc': '=',
                'gallery': '@',
                'title': '@',
                'size': '@',
                'config': '='
            }
        };
    }

    /** @ngInject */
    function w3ImageDialogController($log, $mdMedia, $mdDialog)
    {
        /* jshint validthis: true */
        var vm = this;

        vm.path = "";
        vm.pathSrc = "";
        vm.title = "";
        vm.gallery = "";


        vm.showpicker = function (ev)
        {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));

            var config = angular.isObject(vm.config) ? vm.config : {params:{}};
            config.params.gallery = vm.gallery;

            $log.debug('w3ImageDialogDirective:showpicker', config);

            $mdDialog.show({
                controller: DialogController,
                controllerAs: 'modal',
                templateUrl: '/app/external/w3/components/w3-uploud/dialog-file-uploud.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                resolve: {
                    path: function () {
                        return vm.path;
                    },
                    pathSrc: function () {
                        return vm.pathSrc;
                    },
                    title: function () {
                        return vm.title;
                    },
                    config: function () {
                        return config;
                    }
                }
            }).then(function (result) {
                vm = angular.extend(vm, result);
                
                if(vm.size){
                    vm.path = vm.path.replace('-large', '-' + vm.size);
                    vm.pathSrc = vm.pathSrc.replace('-large', '-' + vm.size);
                }
            });

        };
    }

    /** @ngInject */
    function DialogController($mdDialog, w3Upload, path, pathSrc, title, config)
    {
        /* jshint validthis: true */
        var modal = this;

        //Data
        modal.files = {};
        modal.title = title;
        modal.pathSrc = pathSrc;
        modal.path = path;

        //Methods
        modal.close = close;
        modal.clean = clean;
        modal.upload = upload;

        //------------------------------
        function clean()
        {
            modal.pathSrc = null;
            modal.path = null;
            modal.files = null;
            close();
        }

        function close()
        {
            $mdDialog.hide({
                'path': modal.path,
                'pathSrc': modal.pathSrc
            });
        }

        function upload()
        {
            w3Upload.uploadToRapiPlugin(modal.file, null, config).then(function (result) {
                modal.pathSrc = result.path_large_src;
                modal.path = result.path_large;
                close();
            });
        }

    }

})();
