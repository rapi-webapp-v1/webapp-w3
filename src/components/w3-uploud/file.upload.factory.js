(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .factory('w3Upload', uploadFactory);

    /** @ngInject */
    function uploadFactory($rootScope, Upload, $q, $log, w3Utils, w3Config) {

        var headersUpload = {
            'Content-Type': 'application/json'
        };
        var responses = [];

        var service = {
            uploadToRapiPlugin: uploadRapi,
            uploadAllToRapiPlugin: uploadAllRapi
        };

        return service;
        //---------------------------------------

        function uploadServer(file, url_server, fields_server)
        {
            file.progress = 0;
            fields_server = fields_server || {};
            
            $log.debug(url_server, fields_server);
            
            if(!angular.isObject($rootScope.w3Reload)){
                $rootScope.w3Reload = {};
            }
            
            $rootScope.w3Reload['uploadRapi'] = true;

            return Upload.upload({
                url: url_server,
                fields: fields_server,
                headers: getHeaders(),
                file: file
            }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                file.progress = progressPercentage;
            }).then(function (result) {
                file.progress = 100;
                responses.push(result.data.data);
                $rootScope.w3Reload['uploadRapi'] = false;
                return result.data.data;
            });
        }

        function uploadAll(files, url, fields_server)
        {
            var tasks = [];
            responses = [];

            files.forEach(function (file) {
                tasks.push(uploadServer(file, url, fields_server));
            });

            return $q.all(tasks).then(function () {
                return responses;
            });
        }

        function getHeaders()
        {
            return headersUpload;
        }

        function getParams(merge)
        {
            /**
             *  name: null, //'demo'  nome do arquivo (opcional)        
             *  gallery: null, //nome da galeria (required)  
             *  token: null //token de acesso  (required) (vai depender das configurações em Config::get('rapi_upload.token')
             */
            var config = w3Config.get('upload', config);
            return angular.extend(config, merge);
        }

        function uploadRapi(file, fields_server, config)
        {
            var data = resolveParams(fields_server, config);
            var url = resolveUrl(config);
            return uploadServer(file, url, data);
        }

        function uploadAllRapi(files, fields_server, config)
        {
            var data = resolveParams(fields_server, config);
            var url = resolveUrl(config);
            return uploadAll(files, url, data);
        }

        function resolveUrl(config)
        {
            return config && config.url ? config.url : w3Utils.urlApi('/rapi/fuse/uploads');
        }
        
        function resolveParams(fields_server, config)
        {
            var data = fields_server || {};
            
            if(config && angular.isObject(config.params)){
                data = angular.extend(data, config.params);
            }
            
            return getParams(data);
        }
    }



})();
