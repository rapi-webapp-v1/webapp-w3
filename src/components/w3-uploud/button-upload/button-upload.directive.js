(function() {
  'use strict';

  angular
    .module('rapi.w3')
    .directive('w3ButtonUpload', w3ButtonUploadDirective)
    .controller('w3ButtonUploadController', w3ButtonUploadController);

  /**
   * Gera o botão de upload com atributo model
   *
   * use: <w3-button-upload path="vm.row.logo" path-src="vm.row.logo_src" gallery="nomeGaleria" config="configUpload"></w3-button-upload>
   *
   *  Modelo de configuração
   *  config = {
   *      url: w3Utils.urlApi('/uploads'),
   *      params: {
   *          'owner_type': 'LDM\\Casal\\Casal',
   *          'owner_id': null
   *      }
   *  }
   *
   @ngInject */
  function w3ButtonUploadDirective() {
    return {
      restrict: 'E',
      templateUrl: '/app/external/w3/components/w3-uploud/button-upload/button-upload.html',
      controller: 'w3ButtonUploadController as vm',
      scope: {
        'path': '=',
        'pathSrc': '=',
        'gallery': '@',
        'title': '@',
        'size': '@',
        'config': '='
      }
    };
  }

  /** @ngInject */
  function w3ButtonUploadController($log, $scope, w3Upload) {
    /* jshint validthis: true */
    var vm = this,
      config = {};

    // Data
    vm.files = null;
    vm.title = null;

    // Methods
    vm.clean = clean;
    vm.upload = upload;

    active();

    //-------------------------------
    function active() {
      vm.title = $scope.title || "Selecionar arquivo";

      var defaultConfig = {
        params: {}
      };

      config = angular.isObject($scope.config) ? $scope.config : defaultConfig;

      if ($scope.gallery) {
        config.params.gallery = $scope.gallery;
      }
    }

    function clean() {
      $scope.pathSrc = null;
      $scope.path = null;
      vm.files = null;
    }

    function successUpload(result) {
      var key = $scope.size ? 'path_' + $scope.size : 'path';
      var keySrc = key + '_src';

      $scope.pathSrc = result[keySrc];
      $scope.path = result[key];
    }

    function upload() {
      w3Upload.uploadToRapiPlugin(vm.file, null, config).then(function(result) {
        successUpload(result);
      });
    }


  }



})();
