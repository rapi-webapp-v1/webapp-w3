
(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .directive('w3FilesDrop', w3FilesDropDirective)
            .controller('w3FilesDropController', w3FilesDropController);


    /**
     * Permite selecionar mais de uma imagem e retorna um array de #uploadTransformer
     * 
     * use: <w3-files-drop config="vm.config" gallery="vm.gal" on-uploaded="vm.files"></w3-files-drop>
     @ngInject */
    function w3FilesDropDirective()
    {
        return {
            restrict: 'E',
            templateUrl: '/app/external/w3/components/w3-uploud/file-uploud-drop.html',
            controller: 'w3FilesDropController as vm',
            bindToController: true,
            scope: {
                'onUploaded': '&',
                'config': '=',
                'gallery': '='
            }
        };
    }

    /** @ngInject */
    function w3FilesDropController($log, w3Upload)
    {
        /* jshint validthis: true */
        var vm = this;

        //Data
        vm.files = [];

        //Methods
        vm.addFiles = addFiles;
        vm.removeFiles = removeFiles;
        vm.upload = upload;
        vm.reset = reset;

        //------------------------------
        function upload()
        {
            var config = {
                'params': {'gallery': vm.gallery}
            };

            console.log(vm.config);
            config = angular.merge(config, vm.config);

            console.log(config);

            w3Upload.uploadAllToRapiPlugin(vm.files, null, config).then(function (result) {
                reset();
                handleCallback(result)
            });
        }

        function handleCallback(files)
        {
            if (angular.isFunction(vm.onUploaded)) {
                vm.onUploaded({'$files': files});
            } else {
                $log.error('w3FilesDrop:onUploaded não é uma função');
            }
        }

        function reset()
        {
            vm.files = [];
        }

        function addFiles(files)
        {
            angular.forEach(files, function (file) {
                file.progress = 0;
                vm.files.push(file);
            });
        }

        function removeFiles($index)
        {
            vm.files.splice($index, 1);
        }

    }

})();