(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .directive('w3ImagePicker', imagePickerDirective)
            .directive('w3FileUploudDrop', fileUploudDropDirective)
            .controller('imagePickerController', imagePickerController)
            .controller('fileUploudController', fileUploudController);

    /**
     * Permite selecionar mais de uma imagem e retorna um array de #uploadTransformer
     * use: <rp-file-uploud-drop model="vm.row.logo" config="configUpload"></rp-file-uploud-drop>
     @ngInject */
    function fileUploudDropDirective() 
    {
        return {
            restrict: 'E',
            templateUrl: '/app/external/w3/components/w3-uploud/file-uploud-drop.html',
            controller: 'fileUploudController as vm',
            bindToController: true,
            scope: {
                'model': '=',
                'config': '='
            }
        };
    }

    /**
     * use: <rp-image-picker path="vm.row.logo" config="configUpload" path-src="vm.row.logo_src"></rp-image-picker>
     @ngInject  */
    function imagePickerDirective()
    {
        return {
            restrict: 'E',
            templateUrl: '/app/external/w3/components/w3-uploud/button-upload.html',
            controller: 'imagePickerController as vm',
            bindToController: true,
            scope: {
                'config': '=',
                'path': '=',
                'pathSrc': '=',
                'title': '@'
            }
        };
    }

    /** @ngInject */
    function fileUploudController(w3Upload, $log) {

        /* jshint validthis: true */
        var vm = this;

        //Data
        vm.files = [];
        vm.model = [];

        //Methods
        vm.addFiles = addFiles;
        vm.removeFiles = removeFiles;
        vm.upload = upload;
        vm.reset = reset;
        
        $log.warn('w3FileUploudDrop is deprected use <w3-button-upload config="vm.config" gallery="vm.gal" on-uploaded="vm.files"></w3-button-upload>');

        //------------------------------
        function upload() 
        {
            w3Upload.uploadAllToRapiPlugin(vm.files, vm.config).then(function (result) {
                reset();
                vm.model = result;
            });
        }

        function reset() 
        {
            vm.files = [];
        }

        function addFiles(files) 
        {
            angular.forEach(files, function (file) {
                file.progress = 0;
                vm.files.push(file);
            });
        }

        function removeFiles($index)
        {
            vm.files.splice($index, 1);
        }

    }

    /** @ngInject */
    function imagePickerController($mdMedia, $mdDialog, $log)
    {       
        /* jshint validthis: true */
        var vm = this;

        vm.path = "";
        vm.pathSrc = "";
        vm.title = "";
        
        $log.warn('w3ImagePicker directive is deprected! Use w3ImageDialog');

        vm.showpicker = function (ev)
        {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));

            $mdDialog.show({
                controller: DialogController,
                controllerAs: 'modal',
                templateUrl: '/app/external/w3/components/w3-uploud/dialog-file-uploud.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                resolve: {
                    path: function () {
                        return vm.path;
                    },
                    pathSrc: function () {
                        return vm.pathSrc;
                    },
                    title: function () {
                        return vm.title;
                    },
                    config: function () {
                        return vm.config;
                    }
                }
            }).then(function (result) {
                vm = angular.extend(vm, result);
            });

        };
    }
    
    /** @ngInject */
    function DialogController($mdDialog, w3Upload, path, pathSrc, title, config)
    {
        /* jshint validthis: true */
        var modal = this;

        //Data
        modal.files = {};
        modal.title = title;
        modal.pathSrc = pathSrc;
        modal.path = path;

        //Methods
        modal.close = close;
        modal.clean = clean;
        modal.upload = upload;

        //------------------------------
        function clean() 
        {
            modal.pathSrc = null;
            modal.path = null;
            modal.files = null;
            close();
        }

        function close() 
        {
            $mdDialog.hide({
                'path': modal.path,
                'pathSrc': modal.pathSrc
            });
        }

        function upload() 
        {
            w3Upload.uploadToRapiPlugin(modal.file, config).then(function (result) {
                modal.pathSrc = result.path_large_src;
                modal.path = result.path_large;
                close();
            });
        }

    }

})();
