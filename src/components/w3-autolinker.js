(function() {
    'use strict';

angular
    .module('rapi.w3')

    .directive('w3Autolinker', autoLinkerDiretive)
    .filter('w3Autolinker', autoLinkerFilter);

    autoLinkerDiretive.$inject = ['$timeout'];

    function autoLinkerDiretive($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element) {
                $timeout(function() {
                    var eleHtml = element.html();

                    if (eleHtml === '') {
                        return false;
                    }

                    var text = Autolinker.link(eleHtml, {
                        className: 'autolinker',
                        newWindow: false
                    });

                    text = text.replace(/\n\r?/g, '<br />');

                    element.html(text);

                    var autolinks = element[0].getElementsByClassName('autolinker');

                    for (var i = 0; i < autolinks.length; i++) {
                        angular.element(autolinks[i]).bind('click', function(e) {
                            var href = e.target.href;

                            if (href) {
                                //window.open(href, '_system');
                                window.open(href, '_blank');
                            }

                            e.preventDefault();
                            return false;
                        });
                    }
                }, 0);
            }
        }
    }

    function autoLinkerFilter() {

        return function(input) {

            input = input || '';

            if (input === '') return false;

            var text = Autolinker.link(input, {
                className: 'autolinker',
                newWindow: false
            });

            text = text.replace(/\n\r?/g, '<br />');

            return text;

            //var autolinks = element[0].getElementsByClassName('autolinker');
            //
            //for (var i = 0; i < autolinks.length; i++) {
            //    angular.element(autolinks[i]).bind('click', function(e) {
            //        var href = e.target.href;
            //        console.log('autolinkClick, href: ' + href);
            //
            //        if (href) {
            //            //window.open(href, '_system');
            //            window.open(href, '_blank');
            //        }
            //
            //        e.preventDefault();
            //        return false;
            //    });
            //}
        }
    }

})();
