(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .controller('w3InputCityController', w3InputCityController)
            .directive('w3InputCity', w3InputCityDirective);

    /**
     * EX: <w3-input-city ng-model="vm.row.city_id" w3-city="vm.row.city" w3-open-create="false" flex-gt-xs="75"></w3-input-city>
     *
     * */
    /** @ngInject */
    function w3InputCityDirective() {
        return {
            restrict: 'E',
            require: 'ngModel',
            scope: {
                ngModel: '=',
                city: '=w3City',
                openCreate: '@w3OpenCreate'
            },
            templateUrl: '/app/external/w3/components/w3-input-city/w3-input-city.tpl.html',
            controller: 'w3InputCityController as vm'
        };
    }

    /** @ngInject */
    function w3InputCityController($scope, City, $log) {
        var vm = this;

        // Data
        vm.archive = [];

        // Methods
        vm.querySearch = querySearch;
        vm.selectedItemChange = selectedItemChange;
        vm.showCreateCity = showCreateCity;

        activate();
        //-----------------------

        $scope.$watch('city', setCity);

        function setCity(obj) {
            vm.selectedItem = obj;
            if (obj) {
                vm.selectedItem.city_name = obj.city_name + ", " + obj.state.state_code + ' - ' + obj.state.country.country_code;
            }
        }

        function activate() {
            
             $log.warn('Deprecation warning: w3InputCity is deprecated, use <rf-input-city ng-model="vm.row.city_id" rf-city="vm.row.city" rf-open-create="false" flex-gt-xs="75"></rf-input-city>');
            vm.openCreate = ($scope.openCreate === 'false') ? false : true;
        }

        function querySearch(query) {
            return City.search({
                'q': query,
                'take': 5,
                'sort': 'city_name',
                'include': 'state.country'
            });
        }

        function selectedItemChange(item) {

            $scope.$evalAsync(function () {
                $scope.ngModel = (item) ? item.id : null;
            });

        }

        function showCreateCity(ev) {
            var data = {
                'city_name': vm.searchText,
                'include': 'state.country'
            };
            City.dialog.create([], ev, data).then(function (result) {
                if (result)
                    setCity(result);

            });
        }

    }

})();
