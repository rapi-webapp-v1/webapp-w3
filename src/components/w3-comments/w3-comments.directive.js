(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .directive('w3Comments', Comments)
            .controller('w3CommentsController', w3CommentsController);

    /**
     * USAGE:
     * <w3-comments w3-token="vm.row.token_morph.token"></w3-comments>
     *
     * @constructor
     */
    function Comments() {
        return {
            restrict: 'E',
            scope: {
                token: "=w3Token"
            },
            controller: 'w3CommentsController as vm',
            templateUrl: '/app/external/w3/components/w3-comments/comments.tmpl.html'
        };
    }

    /** @ngInject */
    function w3CommentsController($scope, RapiComment, $log) {

        var vm = this;

        vm.rowComment = {
            'token_morph': null,
            'message': '',
            'type': 'mensagem',
            'include': 'profile'
        };
        vm.comments = [];

        /*  methods */
        vm.addNewComment = addNewComment;

        /////////////////////////////////////////////
        $scope.$watch('token', activate);

        function activate()
        {
             $log.warn('Deprecation warning: w3Comments is deprecated, use <rf-comments rf-token="vm.row.token_morph.token"></rf-comments>');
            
            if ($scope.token) {
                loadMessages();
            }
        }

        function loadMessages() {
            var data = {
                'token_morph': $scope.token,
                'include': 'profile',
                'sort': '-created_at'
            };

            RapiComment.search(data).then(function (result) {
                vm.comments = result;
            });
        }

        /**
         * Add new comment
         *
         * @param newCommentText
         */
        function addNewComment() {
            
            var data = angular.copy(vm.rowComment);
            data.token_morph = $scope.token;
            
            RapiComment.save(data).then(function (result) {
                vm.comments = [result].concat(vm.comments);
                vm.rowComment.message = '';
            });

        }

    }

})();
