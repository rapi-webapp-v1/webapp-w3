(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .controller('w3reportDateController', w3reportDateController)
            .directive('w3ReportDate', w3ReportDate);


    /**
     * USAGE:
     * <w3-report-date  w3-extra-options="" w3-date-ini="" w3-date-end="" w3-on-change="" ></w3-report-date>
     * w3-on-change function : usado para disparar funções quando os parâmetros são alterados
     * w3-extra-options => array [{param: 'column_name', label:''}] vm.dt_column para pegar o tipo de data
     * @constructor
     */

    /** @ngInject */
    function w3ReportDate() {
        return {
            restrict: 'E',
            scope: {
                w3OnChange: '&',
                w3DateIni: '=',
                w3DateEnd: '=',
                w3ExtraOptions: '=',
                getClass: "@setClass"
            },
            controller: 'w3reportDateController as vm',
            templateUrl: '/app/external/w3/components/w3-report-date/w3-report-date.tmpl.html'
        };
    }

    /** @ngInject */
    function w3reportDateController($scope, $locale, w3Date)
    {

        var now = new Date();
        var vm = this;

        //Data
        vm.optionsFilter = [];
        vm.dt_start = null;
        vm.dt_end = null;

        //Methods
        vm.setFilter = setFilter;

        activate();
        //------------------- Functions

        $scope.$watch('[vm.dt_start, vm.dt_end, vm.dt_column]', setDates);

        function activate()
        {
            setoptionsFilter();
            setextraOptions();
            
        }

        function setDates()
        {
            if (!vm.dt_start && !vm.dt_end)
                return;

            var dtParams = {
                dt_column: vm.dt_column,
                dt_start: vm.dt_start,
                dt_end: vm.dt_end
            };

            $scope.w3OnChange({'$dtParams': dtParams});
        }

        function setFilter(option)
        {
            var dt;

            switch (option.type)
            {
                case 'now':
                    vm.dt_start = now;
                    vm.dt_end = now;
                    break;

                case 'last_days':
                    vm.dt_start = w3Date.subDays(now, option.param);
                    vm.dt_end = now;
                    break;

                case 'this_month':
                    var date = new Date();
                    vm.dt_start =  new Date(date.getFullYear(), date.getMonth(), 1);
                    vm.dt_end = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    break;

                case 'month':
                    dt = w3Date.subMonthsInteval(now, option.param);
                    vm.dt_start = dt[0];
                    vm.dt_end = dt[1];
                    break;

                case 'year':
                    dt = w3Date.subYearsInteval(now, option.param);
                    vm.dt_start = dt[0];
                    vm.dt_end = dt[1];
                    break;
            }
        }

        function setextraOptions()
        {
            vm.optionsTypeFilter = [
            ];

            angular.forEach($scope.w3ExtraOptions, function (values) {
                vm.optionsTypeFilter.push(values);
                
                //seta valor default do dt_column
                vm.dt_column = vm.optionsTypeFilter[0].param;

            });
        }

        function setoptionsFilter()
        {
            var mesLabel;
            vm.optionsFilter = [
                {
                    "type": "now",
                    "label": "Hoje",
                    "param": null
                },
                {
                    "type": "last_days",
                    "label": "Últimos 7 dias",
                    "param": 7
                },
                {
                    "type": "this_month",
                    "label": "Este mês",
                    "param": 30
                },
                {
                    "type": "last_days",
                    "label": "Últimos 30 dias",
                    "param": 30
                }
            ];

            mesLabel = $locale.DATETIME_FORMATS.MONTH[ (now.getMonth() - 1) ];

            vm.optionsFilter.push({
                "type": "month",
                "label": mesLabel,
                "param": 1
            });

            mesLabel = $locale.DATETIME_FORMATS.MONTH[ (now.getMonth() - 2) ];

            vm.optionsFilter.push({
                "type": "month",
                "label": mesLabel,
                "param": 2
            });

            vm.optionsFilter.push({
                "type": "year",
                "label": now.getFullYear(),
                "param": 0
            });
        }
    }

})();
