(function () {
    'use strict';

    /**
     * Main module of the Rapi/W3
     */
    angular
            .module('rapi.w3', [
                'toastr',
                //'ngFileUpload',
                'rapi.w3.config'
            ]);


})();
