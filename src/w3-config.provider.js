//mytodo importar w3Config do rapi fuse
(function () {
    'use strict';

    angular
            .module('rapi.w3.config', [])
            .provider('w3Config', w3ConfigProvider);

    /** @ngInject */
    function w3ConfigProvider() {

        // Default configuration
        var w3Configuration = {
            'welcome': 'Bem vindo ao Demo Rapi Fuse',
            'welcome_text': "Texto de boas vindas explicando o que é o Demo Rapi Fuse",
            'bg_auth': '/assets/images/backgrounds/march.jpg',
            'logo': null,
            'sigla': 'M',
            'menuOpenFrom': 'gt-xs',
            'title_project': 'Title',
            'host': {
                'baseUrl': '',
                'baseUrlApi': '/api/v1',
                'appUrl': null,
                'baseModule' : null,
                'env': 'local',                
                'headers' : {},
            },
            'layout': {
                //verticalNavigation, verticalNavigationFullwidthToolbar, verticalNavigationFullwidthToolbar2, horizontalNavigation, contentOnly, contentWithToolbar
                'theme': 'verticalNavigation'
            },
            'upload': {
                'token': null,
                'url': null,
                'params': {
                    //'owner_type': 'LDM\\Casal\\Casal',
                    //'owner_id': null,
                    'gallery': null
                }
            },
            'auth': {
                'routeSuccess': 'app.dashboard',
                'type': null,
                'onLoginRequired': function () {
                    window.location = '#!/auth/login';
                }
            }
        };

        // Methods
        this.config = config;
        this.getConfig = getConfig;
        this.configHost = configHost;
        this.configAuth = configAuth;
        this.configUpload = configUpload;
        this.configLayout = configLayout;

        //PLUGIN
        this.plugins = {};
        this.makePlugin = makePlugin;
        this.addPluginProvider = addPluginProvider;
        this.plugin = getPlugin;
        this.configPlugin = configPlugin;//@deprected
        this.addPlugin = addPlugin;//@deprected


        //----------------------------------

        /**
         * Extend default configuration with the given one
         *
         * @param configuration
         */
        function config(configuration)
        {
            w3Configuration = angular.extend({}, w3Configuration, configuration);
        }

        function configHost(configValue)
        {
            angular.extend(w3Configuration.host, configValue);
        }

        function configAuth(configValue)
        {
            angular.extend(w3Configuration.auth, configValue);
        }

        function configLayout(configValue)
        {
            angular.extend(w3Configuration.layout, configValue);
        }

        function configUpload(configValue)
        {
            angular.extend(w3Configuration.upload, configValue);
        }

        /**
         * Returns a config value
         */
        function getConfig(configName, defaul)
        {

            if (angular.isUndefined(w3Configuration[configName])) {
                return defaul;
            }

            return w3Configuration[configName];
        }

        /*
         *  ============ PLUGINS  ===================
         */

        /**
         * @deprecated
         *
         * //mytodo in deprecated desativar addPlugin
         */
        function configPlugin(namePlugin, configValue)
        {
            angular.extend(w3Configuration[namePlugin], configValue);
            throw "configPlugin is deprecated use ..";
        }

        /**
         * @deprecated
         *
         * //mytodo in deprecated desativar addPlugin
         */
        function addPlugin(namePlugin, configValue)
        {
            w3Configuration[namePlugin] = configValue;
            throw "addPlugin is deprecated use addPluginProvider";
        }

        /**
         * Retorna uma instancia do PluginProvider
         *
         * @returns {w3-config_provider_L2.w3ConfigProvider.PluginProvider}
         */
        function makePlugin(name)
        {
            return new PluginProvider(name);
        }

        /**
         * Ativa o plugin no w3Config
         *
         * @param  PluginProvider
         */
        function addPluginProvider(fn)
        {
            var name = fn.getName();
            this.plugins[name] = fn;
        }

        /**
         * Retorna o plugin
         *
         * @param string name
         * @return PluginProvider
         */
        function getPlugin(name)
        {
            var pl = this.plugins[name];

            if (angular.isUndefined(pl)) {
                throw "Plugin não encontrado";
            }

            return pl;
        }

        /**
         * Func. contruturora de plugins
         */
        function PluginProvider(name)
        {
            this.name = name;
            this.vm = this;
            this.info = {
                tutorial: 'Plugin Sem tutorial'
            };
            this.getConfig = getConfig;
        }

        PluginProvider.prototype = {
            getName: function () {
                return this.name;
            },
            tutorial: function () {
                window.console.info(this.info.tutorial);
                return this.vm;
            },
            setTutotial: function (textTutorial) {
                this.info.tutorial = angular.isArray(textTutorial) ? textTutorial.join("\n") : textTutorial;
                return this.vm;
            }
        };



        /**
         * Service
         */
        this.$get = function () {

            var service = {
                get: getConfig,
                set: setConfig,
                setConfigHost: setConfigHost,
                getConfigHost: getConfigHost
            };

            return service;

            //////////

            /**
             * Creates or updates config object
             *
             * @param configName
             * @param configValue
             */
            function setConfig(configName, configValue) {
                w3Configuration[configName] = configValue;
            }

            function setConfigHost(configValue) {
                angular.extend(w3Configuration.host, configValue);
            }

            function getConfigHost() {
                return w3Configuration.host;
            }

        };
    }


})();
