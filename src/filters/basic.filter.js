(function () {
    'use strict';
    angular
            .module('rapi.w3')
            .filter('w3Moments', mommentsFilter)
            .filter('w3Nl2br', nl2brFilter)
            .filter('w3Date2Str', date2StrFilter)
            .filter('w3DatePt', datePtFilter)
            .filter('w3FromDatePt', fromDatePtFilter)
            .filter('w3OnlyDatePt', onlyDatePtFilter)
            .filter('w3DateMask', datemaskFilter)
            .filter('w3FileSize', fileSizeFilter)
            .filter('w3Paginate', w3PaginateService)
            .filter('w3groupBy', groupBy)
            .filter('w3Phone', phone)
            .filter('w3Cep', cep)
            .filter('w3Cpf', cpf)
            .filter('w3Rg', rg)
            .filter('w3Cnpj', cnpj);
    /**
     * Usage
     * var myFile = 5678;
     *
     * {{myText|rpFileSize}}
     *
     * Output
     * "5.54 Kb"
     *
     */
    function fileSizeFilter()
    {
        var units = [
            'bytes',
            'KB',
            'MB',
            'GB',
            'TB',
            'PB'
        ];
        return function (bytes, precision) {

            if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {
                return '?';
            }

            var unit = 0;
            while (bytes >= 1000) {
                bytes /= 1000;
                unit++;
            }

            return bytes.toFixed(+precision) + ' ' + units[unit];
        };
    }



    /** @ngInject */
    function mommentsFilter(w3Utils)
    {
        moment.locale(w3Utils.getLocale());
        return function (input) {
            var v = input || '';
            if (v === '' || v === 'Invalid Date') {
                return '';
            }

            return moment(input).fromNow();
        };
    }

    /**
     * input: 2014-12-26 14:07:17
     * retorna: 26/12/2014 14:07
     */
    /** @ngInject */
    function datePtFilter() {
        return function (input) {
            input = input || '';

            if(angular.isDate(input)){
              input = input.toISOString();
            }

            if (angular.isString(input) === false || input === "0000-00-00 00:00:00" || input.length < 10)
                return;
            var str_date = input.substr(0, 10);
            var str_hora = input.substr(11, 5);
            input = str_date.split('-');
            return input[2] + '/' + input[1] + '/' + input[0] + " " + str_hora;
        };
    }

    /**
     * input: obj date
     * retorna: 26/12/2014
     */
    /** @ngInject */
    function fromDatePtFilter($filter) {
        return function (input) {

            if (!input) {
                return;
            } else {

                input = $filter('date')(input, 'dd/MM/yyyy');
                return input;
            }
        };
    }

    /**
     * input: 2014-12-26 14:07:17
     * retorna: 26/12/2014 14:07
     */
    /** @ngInject */
    function onlyDatePtFilter()
    {
        return function (input) {
            input = input || '';
            if (angular.isDate(input)) {
                input = input.toString();
            } else if (input === "0000-00-00 00:00:00" || input.length < 10) {
                return;
            }

            var str_date = input.substr(0, 10);
            input = str_date.split('-');
            return input[2] + '/' + input[1] + '/' + input[0];
        };
    }

    /** @ngInject */
    function date2StrFilter(w3Date)
    {
        return function (input) {
            if (angular.isDate(input)) {
                return w3Date.dateToPt(input);
            }
        };
    }

    /** @ngInject */
    function datemaskFilter($log)
    {
        //mytodo delete este filter in 30 dias => 20/09/2016
        var func = datePtFilter();
        return function (input) {
            $log.warn('Filter:w3DateMask is depecreted use w3DatePt');
            return func(input);
        };
    }

    function nl2brFilter()
    {
        return function (data) {
            if (!data)
                return data;
            return data.replace(/\n\r?/g, '<br />');
        };
    }


    /**
     * Use: {{vm.pagination|w3Paginate:'produtos'}}
     * @returns 'Exibindo 1 à 10 produtos de 100.'
     */
    /** @ngInject */
    function w3PaginateService($translate)
    {

        return function (pagination, label) {

            if (!angular.isObject(pagination))
                return;
            var start, end;
            label = label || 'itens';
            start = ((pagination.current_page - 1) * pagination.per_page) + 1;
            end = start + pagination.count - 1;
            //"FILTER_PAGINATE" : "Exibindo {{start}} à {{end}} itens de {{total}}."
            var str = $translate.instant('RAPI.FILTER_PAGINATE', {'start': start, 'end': end, 'total': pagination.total});
            return str;
        };
    }

    /*
     * Ordena uma lista por um parâmetro
     */
    /** @ngInject */
    function groupBy($parse)
    {
        return _.memoize(
                function (items, field) {
                    var getter = $parse(field);
                    return _.groupBy(items, function (item) {
                        return getter(item);
                    });
                }
        );
    }

    /**
     * input: 4535411523
     * retorna: (45) 3541-1523
     */
    /** @ngInject */
    function phone() {
        return function (input) {
            var str = input + '';
            str = str.replace(/\D/g, '');
            if (str.length === 11) {
                str = str.replace(/^(\d{2})(\d{5})(\d{4})/, '($1) $2-$3');
            } else {
                str = str.replace(/^(\d{2})(\d{4})(\d{4})/, '($1) $2-$3');
            }
            return str;
        };
    }


    /**
     * input: 85875000
     * retorna: 85.875-000
     */
    /** @ngInject */
    function cep() {
        return function (input) {
            var str = input + '';
            str = str.replace(/\D/g, '');
            str = str.replace(/^(\d{2})(\d{3})(\d)/, "$1.$2-$3");
            return str;
        };
    }


    /**
     * input: 11573175536
     * retorna: 115.731.755-36
     */
    /** @ngInject */
    function cpf() {
        return function (input) {
            var str = input + '';
            str = str.replace(/\D/g, '');
            str = str.replace(/(\d{3})(\d)/, "$1.$2");
            str = str.replace(/(\d{3})(\d)/, "$1.$2");
            str = str.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
            return str;
        };
    }

    /**
     * input: 11573175536
     * retorna: 115.731.755-36
     */
    /** @ngInject */
    function rg() {
        return function (input) {
            var str = input + '';
            str = str.replace(/\D/g, '');
            str = str.replace(/(\d{2})(\d)/, "$1.$2");
            str = str.replace(/(\d{3})(\d)/, "$1.$2");
            str = str.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
            return str;
        };
    }


    /**
     * input: 52376524000185
     * retorna: 52.376.524/0001-85
     */
    /** @ngInject */
    function cnpj() {
        return function (input) {
            var str = input + '';
            str = str.replace(/\D/g, '');
            str = str.replace(/^(\d{2})(\d)/, '$1.$2');
            str = str.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3');
            str = str.replace(/\.(\d{3})(\d)/, '.$1/$2');
            str = str.replace(/(\d{4})(\d)/, '$1-$2');
            return str;
        };
    }

})();
