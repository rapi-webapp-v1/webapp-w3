(function () {
    'use strict';

    angular
            .module('rapi.w3.plugin.rollbar', [
                'rapi.w3.config',
                'tandibar/ng-rollbar'
            ])
            .config(config)
            .run(runBlock);


    /** @ngInject */
    function config(w3ConfigProvider, RollbarProvider)
    {
        var rollbarPlugin = w3ConfigProvider.makePlugin('rollbar');

        rollbarPlugin.init = function (token)
        {
            RollbarProvider.init({
                accessToken: token,
                captureUncaught: true
            });
        };

        var turorial = [
            '###BOWER INSTAL:',
            'bower install ng-rollbar --save',
            '###index.config:',
            'import module [rapi.w3.plugin.rollbar]',
            'w3ConfigProvider.plugin(\'rollbar\').init(\'YOUR_TOKEN\')',
            'https://github.com/tandibar/ng-rollbar'
        ];

        rollbarPlugin.setTutotial(turorial);

        w3ConfigProvider.addPluginProvider(rollbarPlugin);
    }

    /** @ngInject */
    function runBlock($rootScope, $log, Rollbar, w3Auth, w3Config)
    {
        var env = w3Config.get('host').env;

        var configure = function (profile) {

            var payload = {
                environment: env,
                person: {
                    id: 0,
                    username: "Guest"
                }
            };

            if (profile) {
                payload.person = {
                    id: profile.user_id,
                    username: profile.name
                };
            }

            $log.info('Rollbar.configure', payload);

            Rollbar.configure({
                payload: payload
            });
        };

        if (env === 'local') {
            Rollbar.disable();
        } else {
            configure(w3Auth.profile());
            $rootScope.$on('w3Auth:onChangeProfile', configure);
        }
        
    }

})();