(function () {
    'use strict';

    angular
            .module('rapi.w3')
            .config(w3Config);


    /** @ngInject */
    function w3Config($httpProvider, toastrConfig, w3HttpInterceptorProvider) {

        toastrConfig.allowHtml = true;


        $httpProvider.interceptors.push('w3HttpInterceptor');
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.withCredentials = false;
        $httpProvider.defaults.headers.common['Content-Type'] = 'application/json';



        w3HttpInterceptorProvider.tokenGetter = ['w3Auth', function (w3Auth) {
                return w3Auth.getToken();
            }];

        //var host = w3ConfigProvider.getConfig('host');
        //console.log('W#@', host);
        //w3HttpInterceptorProvider.prependHeaders = host.headers;
    }


})();
