(function () {
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
            .module('rapi.w3')
            .run(runBlock);


    /** @ngInject */
    function runBlock($rootScope, w3Auth)
    {
        $rootScope.$on('w3HttpInterceptor:loginRequired', function () {
            w3Auth.setToken(null);
        });
    }

})();